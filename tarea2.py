"""Calculates the word count of the given file.

the file can be local or if you setup cluster.

It can be hdfs file path"""
#enconding: utf-8
## Imports
from pyspark.ml.classification import LogisticRegression
from pyspark import SparkConf, SparkContext

from operator import add
import sys
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.sql.functions import mean,col,split, col, regexp_extract, when, lit
from pyspark.ml.feature import StringIndexer
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import QuantileDiscretizer


## Constants
APP_NAME = " HelloWorld of Big Data"
##OTHER FUNCTIONS/CLASSES
def sexTransformMapper(elem):
    '''Function which transform "male" into 1 and else things into 0
    - elem : string
    - return : vector
    '''
     
    if elem == 'male' :
        return [0]
    else :
        return [1]

def null_value_count(df):
  null_columns_counts = []
  numRows = df.count()
  for k in df.columns:
    nullRows = df.where(col(k).isNull()).count()
    if(nullRows > 0):
      temp = k,nullRows
      null_columns_counts.append(temp)
  return(null_columns_counts)

def main(sc,filename):
   spark = SparkSession \
    .builder \
    .appName("Spark ML example on titanic data ") \
    .getOrCreate()

   s3_bucket_path= filename
   titanic_df = spark.read.csv(s3_bucket_path,header = 'True',inferSchema='True')
   passengers_count = titanic_df.count()
   print(passengers_count)

   titanic_df.show(5)
   titanic_df.describe().show()
   titanic_df.printSchema()
   titanic_df.select("Survived","Pclass","Embarked").show()
   titanic_df.groupBy("Survived").count().show()
   titanic_df.groupBy("Sex","Survived").count().show()
   titanic_df.groupBy("Pclass","Survived").count().show()
   null_columns_count_list = null_value_count(titanic_df)
   spark.createDataFrame (null_columns_count_list, ['Column_With_Null_Value', 'Null_Values_Count']).show()
   mean_age = titanic_df.select(mean('Age')).collect()[0][0]
   print(mean_age)
   titanic_df = titanic_df.withColumn("Initial",regexp_extract(col("Name"),"([A-Za-z]+)\.",1))
   titanic_df.show()
   titanic_df.select("Initial").distinct().show()
   titanic_df = titanic_df.replace(['Mlle','Mme', 'Ms', 'Dr','Major','Lady','Countess','Jonkheer','Col','Rev','Capt','Sir','Don'],
               ['Miss','Miss','Miss','Mr','Mr',  'Mrs',  'Mrs',  'Other',  'Other','Other','Mr','Mr','Mr'])
   feature = VectorAssembler(inputCols=titanic_df.columns[1:],outputCol="features")
   feature_vector= feature.transform(titanic_df)

   (trainingData, testData) = feature_vector.randomSplit([0.8, 0.2],seed = 11)
   lr = LogisticRegression(labelCol="Survived", featuresCol="features")
#Training algo
   lrModel = lr.fit(trainingData)
   lr_prediction = lrModel.transform(testData)
   lr_prediction.select("prediction", "Survived", "features").show()
   evaluator = MulticlassClassificationEvaluator(labelCol="Survived", predictionCol="prediction", metricName="accuracy")

#   textRDD = sc.textFile(filename)
#   words = textRDD.flatMap(lambda x: x.split(' ')).map(lambda x: (x, 1))
#   wordcount = words.reduceByKey(add).collect()
#   for wc in wordcount:
#      print wc[0],wc[1]

if __name__ == "__main__":

   # Configure Spark
   conf = SparkConf().setAppName(APP_NAME)
   conf = conf.setMaster("local[*]")
   sc   = SparkContext(conf=conf)
   filename = sys.argv[1]
   # Execute Main functionality
   main(sc, filename)
